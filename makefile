all: main

main: higher_order_functions.c linked_list.c  main.c
	gcc -o -ansi -pedantic -Wall -Werror main linked_list.c higher_order_functions.c main.c

clean: 
	rm -f main
