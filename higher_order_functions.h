#ifndef _HIGHER_ORDER_FUNCTIONS_
#define _HIGHER_ORDER_FUNCTIONS_


#include "linked_list.h"
#include <stdbool.h>



void foreach(list_node* head, void (*func)(int));
list_node* map(list_node* head, int (*func)(int));
int foldl(int acc, int (*func)(int, int), list_node* head);
void map_mut(list_node* head, int (*func)(int));
list_node* iterate(int s, int size, int (*func)(int));

#endif
