#include "higher_order_functions.h"
#include "linked_list.h"
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>


//f
list_node* read_input(int count){
	int value;
	char character=' ';
	list_node* list = 0;
	int counter = 0;
	if(count == 0) return list;
	while(counter<count && scanf("%d%c", &value, &character)!=EOF){
		if(list == 0) list = create_list(value);
		else list_add_back(value, &list);
		counter++;
	}
	return list;
}

void print_int_sp(int value){
	printf("%d ", value);
}

void print_int_nl(int value){
	printf("%d\n", value);
}

int sqr(int value){
	return value*value;
}

int cube(int value){
	return value*value*value;
}

int sum(int a, int b){
	return a+b;
}

int get_max(int a, int b){
	if(a>b) return a;
	return b;
}

int get_min(int a, int b){
	if(a>b) return b;
	return a;
}

int abs(int value){
	if(value<0) return -value;
	return value;
}

int dl(int a){
	return 2*a;
}

char* read_filename(char* message){
	char* filename;	
	bool error = false;
	while(1){
		char* symbol;
		int counter = 0;
		int c;
		filename = malloc(sizeof(char) * 20);
		if(error){
			while((c=(char)getchar())!='\n' && c!=EOF);
		}
		printf("%s", message);
		symbol = filename;
		while((*symbol++=getchar())!='\n'){
			counter++;
			if(counter > 19)  break;
		}
		if(counter>19) {
			printf("The name of file is too long.\n");
			error = true;
			}
		else {
			*symbol = '\0';
			break;
		}	
	}
	return filename;
		
	
}

int main(void){
	int count;
	printf("Write a count of integers:\n");
	scanf("%d",&count);
	if(count>0) printf("Write your numbers:\n");
	list_node* list = read_input(count);
	if(list == 0) {
		printf("Invalid input\n");
		return 0;
	}
	printf("Results of foreach:\n");
	foreach(list, print_int_sp);
	printf("\n");
	foreach(list, print_int_nl);
	printf("Results of map:\n");
	foreach(map(list, sqr), print_int_sp);
	printf("\n");
	foreach(map(list, cube), print_int_sp);
	printf("\n");
	printf("Results of foldl:\n");
	printf("sum = %d\n",foldl(0, sum, list));	
	printf("max = %d\n",foldl(INT_MIN, get_max, list));
	printf("min = %d\n",foldl(INT_MAX, get_min, list));
	map_mut(list, abs);
	printf("Result of mut:\n");
	foreach(list, print_int_sp);
	printf("\n");
	printf("Result of iterate:\n");
	list_node* list_of_powers = iterate(2, 10, dl);
	foreach(list_of_powers, print_int_sp);
	printf("\n");
	char* filename = read_filename("Write the name of file:\n");
	if (save(list, filename)==true) printf("Save completed");
	else printf("Save error");
	printf("\n");
	if (load(&list, filename)==true) printf("Load completed");
	else printf("Load error");
	printf("\n");
	foreach(list, print_int_sp);
	printf("\n");
	filename = read_filename("Write the name of binary file:\n");
	if (serialize(list, filename)==true) printf("Serialize completed");
	else printf("Serialize error");
	printf("\n");
	if (deserialize(&list, filename)==true) printf("Deserialize completed");
	else printf("Deserialize error");
	printf("\n");
	foreach(list, print_int_sp);
	printf("\n");
	list_free(list_of_powers);
	list_free(list);

	
	
	
}
