#include "linked_list.h"
#include "higher_order_functions.h"
#include <stdbool.h>


void foreach(list_node* head, void (*func)(int)){
	int size = list_length(head);
	int i;
	for (i=0; i<size; i++){
		func(list_get(i, head));	
	}
}

list_node* map(list_node* head, int (*func)(int)){
	int size = list_length(head);
	int i;
	list_node* new_list = create_list(func(list_get(0, head)));
	for (i=1; i<size; i++){
		list_add_back(func(list_get(i, head)), &new_list);
	}
	return new_list;
}

int foldl(int acc, int (*func)(int, int), list_node* head){
	int size = list_length(head);
	int i;
	for(i=0; i<size; i++){
		acc = func(acc, list_get(i, head));
	}
	return acc;
}

void map_mut(list_node* head, int (*func)(int)){
	int size = list_length(head);
	int i;
	for(i=0; i<size; i++){
		int new_value = func(list_get(i, head));
		list_node* current = list_node_at(i, head);
		current->value = new_value;
	}
}

list_node* iterate(int s, int size, int (*func)(int)){
	int i;
	list_node* new_list = create_list(s);
	for(i=1; i<size; i++){
		list_add_back(func(list_get(i-1, new_list)), &new_list);
	}

	return new_list;
}
