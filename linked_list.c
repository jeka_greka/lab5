#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>
#include "linked_list.h"


list_node* create_list(int number){
	list_node* new_list = (list_node*) malloc(sizeof(list_node));
	new_list->value = number;
	new_list->next = 0;
	return new_list;
}

list_node* list_add_front(int number, list_node** head){
	list_node* new_head = create_list(number);
	new_head->next = *head;
	*head = new_head;
	return new_head;
	
}

list_node* list_add_back(int number, list_node** head){
	list_node* new_node = create_list(number);
	list_node* last = list_getLast(*head);
	last->next = new_node;
	return new_node;
	
}

list_node* list_node_at(int index, list_node* head){
	list_node* current = head;
	int counter = 0;	
	while(counter <index && current){
		counter++;
		current = current->next;	
	}
	return current;
}

list_node* list_getLast(list_node* head){
	list_node* current = head;
	if(current == 0) return 0;
	while(current->next){
		current = current->next;
	}
	return current;
}

int list_get(int index, list_node* head){
	list_node* current = list_node_at(index, head);
	if(current == 0) return 0;
	return current->value;
}

void list_free(list_node* head){
	list_node* next = head->next;
	while(next){
		free(head);
		head = next;
		next = next->next;
	}
	free(head);

}

int list_length(list_node* head){
	int counter = 0;
	list_node* current = head;
	if(current == 0) return 0;
	while(current){
		counter++;
		current = current->next;
	}
	return counter;
}

long long list_sum(list_node* head){
	list_node* current = head;
	long long sum = 0;
	while(current){
		sum += current->value;
		current = current->next; 
	}
	return sum;
}


int* as_array(list_node* head){
	int size = list_length(head);
	int* a = (int*)malloc(size * sizeof(int));
	int i;
	for(i=0; i<size; i++){
		a[i] = list_get(i, head);	
	}
	return a;
}

bool serialize(list_node* list, const char* filename){
	FILE* f = fopen(filename, "wb");
	int* array = as_array(list);
	int size = list_length(list);
	if(f == NULL) return false;
	int count = fwrite(array, size*sizeof(int), 1, f);
	fclose(f);
	return true;	
}

bool save(list_node* list, const char* filename){
	FILE* f = fopen(filename, "w");
	int* array = as_array(list);
	int size = list_length(list);
	int i;
	if(f == NULL) return false;
	for(i=0; i<size; i++){
		fprintf(f, "%d ", list_get(i, list));
	}
	fclose(f);
	return true;
	
}

bool load(list_node** list, const char* filename){
	FILE* f = fopen(filename, "r");
	int value;
	char character=' ';
	if(f == NULL) return false;
	while(fscanf(f, "%d%c", &value, &character)!=EOF){
		 list_add_back(value, list);
	}
	fclose(f);
	return true;
	
}

bool deserialize(list_node** list, const char* filename){
	FILE* f = fopen(filename, "rb");
	int value;
	if(f == NULL) return false;
	while(fread(&value, sizeof(int), 1, f)==1){
		list_add_back(value, list);
	}
	fclose(f);
	return true;
	
}







